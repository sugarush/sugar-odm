from . controller import Controller


class Set(Controller):

    def __init__(self, *args, **kargs):
        super(Set, self).__init__(*args, **kargs)
        self._valid = self.field.type

    def check(self, value):
        _value = { value }
        intersection = self._valid.intersection(_value)
        if not intersection:
            raise ValueError(f'Invalid set value: {value}')

    def serialize(self, *args, **kargs):
        return self.model._data.get(self.field.name)

    def set(self, value):
        self.check(value)
        self.model._data[self.field.name] = value
