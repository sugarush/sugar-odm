class Acquire(object):

    def __init__(self, model, lock=None, **kargs):
        self.model = model
        self.lock = lock
        self.kargs = kargs

    async def __aenter__(self):
        await self.lock.acquire(**self.kargs)

    async def __aexit__(self, exc_type, exc, tb):
        await self.lock.release()
