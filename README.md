# sugar-odm

[Documentation](https://sugar-odm.docs.sugarush.io)

## Example

```python
from sugar_odm import PostgresDBModel, Field

class Model(PostgresDBModel):
  field = Field()

model = Model({ 'field': 'value' })

await model.save()

print(model.id)
```
