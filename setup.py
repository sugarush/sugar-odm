__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-odm',
    version='0.0.3',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-odm',
    packages=[
        'sugar_odm',
        'sugar_odm.backend',
        'sugar_odm.controller'
    ],
    description='An asynchronous ODM for MongoDB, Postgres and RethinkDB.',
    install_requires=[
        'sugar-asynctest@git+https://gitlab.com/sugarush/sugar-asynctest@master',
        'sugar-concache@git+https://gitlab.com/sugarush/sugar-concache@master',
        'sugar-lock@git+https://gitlab.com/sugarush/sugar-lock@master',
        'inflection',
        'motor',
        'asyncpg',
        'rethinkdb',
        'aioredis',
        'jsonpatch'
    ]
)
